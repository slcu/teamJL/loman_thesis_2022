# Plots a pattern of stress activation/deactivation.
function plot_stress_pattern(addition_times,removal_times,heigth;lw=4,ls=:dot,c1=:red,c2=:black)
    plot()
    foreach(at -> plot!([at,at],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c1), addition_times)
    foreach(rt -> plot!([rt,rt],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c2), removal_times)
    return plot!()
end;
function plot_stress_pattern!(addition_times,removal_times,heigth;lw=4,ls=:dot,c1=:red,c2=:black)
    foreach(at -> plot!([at,at],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c1), addition_times)
    foreach(rt -> plot!([rt,rt],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c2), removal_times)
    return plot!()
end;

# Plots a pattern of stress activation/deactivation and induction.
function plot_stress_induction_pattern(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=4,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end;
function plot_stress_induction_pattern!(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern!(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=4,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end;

# Plots the cummulative activation plots for a single activation simulation.
cummulative_activation_plot(args...;kwargs...) = (plot(); cummulative_activation_plot!(args...;kwargs...);)
function cummulative_activation_plot!(sols,min_thres,max_thres;start=-500,stop=2000,startT=start,stopT=stop,var=1,kwargs...)
    start_idx = findfirst(sols[1].t .== start); stop_idx = findfirst(sols[1].t .== stop);
    startT_idx = findfirst(sols[1].t .== startT); stopT_idx = findfirst(sols[1].t .== stopT);
    distribution = map(i ->  mean(map(sol -> on_value(sol.u[i][var],min_thres,max_thres), sols)), 1:length(sols[1].t))
    return plot!(sols[1].t[startT_idx:stopT_idx], distribution[start_idx:stop_idx]; xlabel="Time (Minutes)", ylabel = "Fraction of activated cells",legend=:bottomright,framestyle=:box,grid=false,guidefontsize=13,labelfontsize=13,ylimit=(-0.015,1.03),tickfontsize=9,right_margin=3mm,title="",titlefontsize=20,kwargs...)
end

# Plots the cummulative activation plots for a set of activation simulations.
cummulative_activations_plot(args...;kwargs...) = (plot(); cummulative_activations_plot!(args...;kwargs...))
function cummulative_activations_plot!(sols,l_levels,idxs,min,max;starts=fill(-500,length(sols)),stops=fill(2000,length(sols)),startsT=starts,stopsT=stops,label_base="Lysozyme concentration",label_end="(au)",kwargs...)
    for idx in idxs
        cummulative_activation_plot!(sols[idx],min,max;start=starts[idx],stop=stops[idx],startT=startsT[idx],stopT=stopsT[idx],label="$(label_base) = $(l_levels[idx]) $(label_end)",kwargs...)
    end
    plot!()
end

# Calculates the degree with which a simulation is "on".
on_value(val,minV,maxV) = max(0,min(1,(val-minV)/(maxV-minV)));

# Plots the fold changes due to various levels of stress.
function plot_fold_changes(inactive_values,active_valuess,l_levels,idxs; xguide="Lysozyme concentration (au)", yguide= "Fold change", kwargs...)
    mean_inactive = mean(inactive_values)
    fold_changes = map(active_values -> active_values ./ mean_inactive, active_valuess)
    return plot(map(i -> l_levels[i], idxs),mean.(fold_changes),ribbon=std.(fold_changes);xguide=xguide,yguide=yguide,kwargs...)
end;