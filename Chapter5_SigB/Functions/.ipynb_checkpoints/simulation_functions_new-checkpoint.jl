### Fetch Packages ###
using DifferentialEquations


### Simulation Functions ###

# Monte Carlo simulation of the model.
function simulate_model(modelE::ModelExpanded,t_stress,l,n;p_changes=[],dt=0.00001,saveat=0.1,t_ss=100.0)
    p = modified_parameters(modelE,p_changes)
    u0s = solve(SDEProblem(modelE.model.system,modelE.u0_func(p),(0.,t_ss),p,noise_scaling=modelE.noises),ImplicitEM(),callback=positive_domain_cb,adaptive=false,saveat=1.,dt=dt).u[floor(Int64,t_ss/2):end]
    sprob = SDEProblem(modelE.model.system,u0s[end],(-t_stress,l),p,noise_scaling=modelE.noises)
    eprob = EnsembleProblem(sprob,prob_func=(p,i,r)->remake(p,u0=rand(u0s)))
    return solve(eprob,ImplicitEM(),trajectories=n,callback=CallbackSet(positive_domain_cb,modelE.stress_cb(0.)),adaptive=false,saveat=saveat,dt=dt,tstops=[0.])
end

# Monte Carlo ssa simulation of the model (igoshin).
function ssa_simulate_model(modelE::ModelExpanded,l,n;u0=zeros(length(modelE.model.system.states)),stress_step=(10,0.0,0),saveat=0.1,save_positions=(true,true),p_changes=[],t_ss=50.0)
    p = modified_parameters(modelE,p_changes)[1:end-1]
    JumpProblem(modelE.model.system,DiscreteProblem(modelE.model.system,u0,(0.,t_ss),p),Direct())
    u0s = solve(JumpProblem(modelE.model.system,DiscreteProblem(modelE.model.system,u0,(0.,t_ss),p),Direct()),SSAStepper()).u[floor(Int64,t_ss/2):end]
    
    dprob = DiscreteProblem(modelE.model.system,u0s[end],(-stress_step[2],l),p)
    jprob = JumpProblem(modelE.model.system,dprob,Direct(),save_positions=save_positions)
    eprob = EnsembleProblem(jprob,prob_func=(p,i,r)->remake(p,u0=rand(u0s)))
    cb = DiscreteCallback((u,t,integrator)->t==0.0,integrator->integrator.u[stress_step[1]]+=stress_step[3],save_positions=(false,false))
    return solve(eprob,SSAStepper(),trajectories=n,callback=cb,tstops=[0.0],saveat=saveat)
end


### Callbacks ###

# Positive domain callback.
positive_domain_cb = DiscreteCallback((u,t,integrator) -> minimum(u) < 0,integrator -> integrator.u .= integrator.uprev,save_positions = (false,false))


### Utility functions ###

# Generates a modified parameter set.
function modified_parameters(modelE::ModelExpanded,p_changes)
    p = deepcopy(modelE.model.p_vals)
    foreach(pc -> p[get_p_idx(modelE,pc[1])] = pc[2], p_changes)
    return p
end

# Finds the index of a parameter in a model.
get_p_idx(modelE::ModelExpanded,par::Symbol) = findfirst(map(i -> Symbol(modelE.parameter_syms[i]) == par, 1:length(modelE.parameter_syms)))
