### Fetch Packages ###
using DifferentialEquations




### Simulation Functions ###

# Monte Carlo simulation of the model.
function simulate_model(model,t_stress,l,n;p_changes=[],dt=0.00001,saveat=0.1,t_ss=100.0)
    p = modified_parameters(model,p_changes)
    u0s = solve(SDEProblem(model.system,model.u0_func(p),(0.,t_ss),p,noise_scaling=model.noises),ImplicitEM(),callback=positive_domain_cb,adaptive=false,saveat=1.,dt=dt).u[floor(Int64,t_ss/2):end]
    sprob = SDEProblem(model.system,u0s[end],(-t_stress,l),p,noise_scaling=model.noises)
    eprob = EnsembleProblem(sprob,prob_func=(p,i,r)->remake(p,u0=rand(u0s)))
    return solve(eprob,ImplicitEM(),trajectories=n,callback=CallbackSet(positive_domain_cb,model.stress_cb(0.)),adaptive=false,saveat=saveat,dt=dt,tstops=[0.])
end

# Monte Carlo ssa simulation of the model (igoshin).
function ssa_simulate_model(model,l,n;u0=zeros(length(model.system.states)),stress_step=(10,0.0,0),saveat=0.1,save_positions=(true,true),p_changes=[],t_ss=50.0)
    p = modified_parameters(model,p_changes)[1:end-1]
    JumpProblem(model.system,DiscreteProblem(model.system,u0,(0.,t_ss),p),Direct())
    u0s = solve(JumpProblem(model.system,DiscreteProblem(model.system,u0,(0.,t_ss),p),Direct()),SSAStepper()).u[floor(Int64,t_ss/2):end]
    
    dprob = DiscreteProblem(model.system,u0s[end],(-stress_step[2],l),p)
    jprob = JumpProblem(model.system,dprob,Direct(),save_positions=save_positions)
    eprob = EnsembleProblem(jprob,prob_func=(p,i,r)->remake(p,u0=rand(u0s)))
    cb = DiscreteCallback((u,t,integrator)->t==0.0,integrator->integrator.u[stress_step[1]]+=stress_step[3],save_positions=(false,false))
    return solve(eprob,SSAStepper(),trajectories=n,callback=cb,tstops=[0.0],saveat=saveat)
end


### Callbacks ###

# Positive domain callback.
positive_domain_cb = DiscreteCallback((u,t,integrator) -> minimum(u) < 0,integrator -> integrator.u .= integrator.uprev,save_positions = (false,false))
