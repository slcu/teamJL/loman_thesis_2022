### Fetch Required Packages ###
using Distributions

### Creates GAMOUR Process ###

#Implements the Gamma distributed Ornstein Uhlenbeck process. Adapted from James Locke.
function makeOUgamma(x, λ, ν, α, T, N_Float64)
    N = Int64(N_Float64)
    Δ = T/N
    pathz = zeros(N+1)
    pathgou = zeros(N+1)

    pathz[1] = 0
    pathgou[1] = x

    for i = 2:(N+1)
        getnext = generateGAMOU(Δ,λ,ν,α)
        pathz[i] = pathz[i-1]+getnext[1]
        pathgou[i] = exp(-λ*Δ)*pathgou[i-1]+getnext[2]
    end
    GOU = (pathz, pathgou)
end
function generateGAMOU(Δ,λ,ν,α)
    pse = rand(Exponential(1/(λ*ν)))
    zpse = 0;
    xpse = 0;
    while (pse <= Δ)
        y = rand(Exponential(1/α))
        zpse = zpse+y;
        xpse = xpse+exp(-λ*(Δ-pse))*y;
        pse = pse+rand(Exponential(1/(λ*ν)));
    end
    getnext = (zpse,xpse);
end


### GAMOU Managment Callbacks ###

# Keeps a target parameter at a gamou value.
function manage_OU_callback_par(target_p,gamOU,gamou_params)
    return DiscreteCallback((u,t,i) -> true,make_affect!_p(target_p,gamOU,gamou_params),save_positions=(false,false));    
end
# Creates affect! function for the parameter based gamou process.
function make_affect!_p(target_p,gamOU,gamou_params)
    return function affect!(integrator)
        step_size = gamou_params[5]/gamou_params[6]
        n = integrator.t/step_size
        integrator.p[target_p] = gamOU[2][floor(Int,n)]
    end
end

# Keeps narula model phosphatase at a gamou value.
function manage_OU_callback_par(gamOU,gamou_params)
    return DiscreteCallback((u,t,i) -> true,make_affect!_narula(gamOU,gamou_params),save_positions=(false,false));    
end
# Creates affect! function for the variable based gamou process.
function make_affect!_narula(gamOU,gamou_params)
    return function affect!(integrator)
        step_size = gamou_params[5]/gamou_params[6]
        n = integrator.t/step_size
        new_p = gamOU[2][floor(Int,n)]
        if (gamOU[2][floor(Int,n)]-integrator.u[9]) > 0
            integrator.u[10] = gamOU[2][floor(Int,n)]-integrator.u[9]
        else
            integrator.u[10] = 0
            integrator.u[9] = gamOU[2][floor(Int,n)]            
        end
    end
end