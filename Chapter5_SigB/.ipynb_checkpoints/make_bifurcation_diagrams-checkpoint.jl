### Fetch Packages  ###
using Catalyst
using OrdinaryDiffEq
using BifurcationKit, Setfield, LinearAlgebra
using Serialization


### Fetch Files ###
include("Functions/models.jl");


### Declare the Model ###
igoshin_bif_system = @reaction_network begin
    kDeg,       (w,w2,w2v,v,w2v2,vP,σB,w2σB) ⟶ ∅
    kDeg,       vPp ⟶ phos
    (kBw,kDw),  2w ⟷ w2
    (kB1,kD1),  w2 + v ⟷ w2v
    (kB2,kD2),  w2v + v ⟷ w2v2
    kK1,        w2v ⟶ w2 + vP
    kK2,        w2v2 ⟶ w2v + vP
    (kB3,kD3),  w2 + σB ⟷ w2σB
    (kB4,kD4),  w2σB + v ⟷ w2v + σB
    (kB5,kD5),  vP + phos ⟷ vPp
    kP,         vPp ⟶ v + phos
    v0*((1+F*σB)/(K+σB)),     ∅ ⟶ σB
    λW*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ w
    λV*v0*((1+F*σB)/(K+σB)),  ∅ ⟶ v
    
    (pStress,1), 0 <--> phos
    1,  vPp ⟶ vP 
end kBw kDw kD kB1 kB2 kB3 kB4 kB5 kD1 kD2 kD3 kD4 kD5 kK1 kK2 kP kDeg v0 F K λW λV pInit pStress;


### Declare Functions ###
function make_bifurcation_diagram(rn,params,p_sym,p_span;v_idx=7)    
    p_idx = findfirst(p_sym .== map(i -> Symbol(rn.ps[i]), 1:length(rn.ps)))
    (make_bifurcation_diagram_confirmed_start(rn,params,p_idx,(p_span[1],params[p_idx]);v_idx=v_idx),make_bifurcation_diagram_confirmed_start(rn,params,p_idx,(params[p_idx],p_span[end]);v_idx=v_idx),p_sym,p_span)
end
function make_bifurcation_diagram_confirmed_start(rn,params,p_idx,p_span;v_idx=7)
    odefun = ODEFunction(convert(ODESystem,rn),jac=true)
    F = (u,p) -> odefun(u,p,0)      
    J = (u,p) -> odefun.jac(u,p,0);

    u0 = solve(ODEProblem(rn,ones(length(rn.states)),(0.,50.),params),Rosenbrock23()).u[end];
    dsmax = params[p_idx]/500
    dsmin = min(params[p_idx]/10000,1e-4)
    ds = (p_span[1]==params[p_idx]) ? sqrt(dsmax*dsmin) : -sqrt(dsmax*dsmin)
    
    opts_br = ContinuationPar(pMin = p_span[1], pMax = p_span[2], dsmax = dsmax, dsmin = dsmin, ds=ds, maxSteps = 1000000,
        detectBifurcation = 3,
        nInversion = 6, maxBisectionSteps = 25,
        nev = 3)

    brs, = continuation(F, J, u0, params, (@lens _[p_idx]), opts_br; tangentAlgo = NaturalPred(),
        recordFromSolution=(x, p) -> x[v_idx], verbosity = 0, plot=false);
    
    return brs
end
function make_bif_diagrams(ps,p_idx,stress_values)
    map(sv -> make_bifurcation_diagram(igoshin_bif_system,setindex!(copy(ps),sv,24),Symbol(igoshin_bif_system.ps[p_idx]),(ps[p_idx]/10.0,ps[p_idx]*10.0)), stress_values)    
end
function compress_bifurcation_diagram(bif_diagram)
    brs = bif_diagram[1:2]
    ps = [reverse(getfield.(brs[1].branch,:param))...,getfield.(brs[2].branch,:param)...]
    xs = [reverse(getfield.(brs[1].branch,:x))...,getfield.(brs[2].branch,:x)...]
    stabs = [reverse(maximum.(real.(getfield.(brs[1].eig,:eigenvals))) .> 0)...,(maximum.(real.(getfield.(brs[2].eig,:eigenvals))) .> 0)...];
    return (ps=ps,xs=xs,stabs=stabs,eq0=(brs[1].branch[1].param,brs[1].branch[1].x),sym=bif_diagram[3],p_span=bif_diagram[4])
end


### Makes the Bifurcation Diagrams  ###
stress_values = [0.05,0.2,0.8]
@time for i = 1:22
    println("Running for: $(igoshin_model.parameter_syms[i])")
    @time bif_diagrams = compress_bifurcation_diagram.(make_bif_diagrams(igoshin_model.parameters,i,stress_values))
    serialize("Data/Bifurcation_diagrams/bifs_$(igoshin_model.parameter_syms[i])_$(igoshin_model.parameters[i]/10.0)_$(igoshin_model.parameters[i]*10.0).jls",bif_diagrams)
end
println("Running for: pStress")
@time bif_diagram = compress_bifurcation_diagram(make_bifurcation_diagram(igoshin_bif_system,igoshin_model.parameters,:pStress,(0.1,10.0));)
serialize("Data/Bifurcation_diagrams/bif_pStress_0.1_10.0.jls",bif_diagram)

println("Finished making bifurcation diagrams.")
