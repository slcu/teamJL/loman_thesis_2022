# Old version
Due to an update to GitLab, I was unable to update this repository. Hence, in the revised version of the thesis the updated repository is referenced. The updated repository can be accesed at https://gitlab.developers.cam.ac.uk/slcu/teamjl/loman_thesis_2022/.

# How bacteria modulate mixed positive/negative feedback loops to create desired phenotypes

This repository contains all files related to Torkel Loman's PhD thesis. This includes files for making simulations, analysing data, and generating plots corresponding to those in the thesis. Files with code contain comments to aid understanding. All code is written in the Julia programming language (version 1.7).

### Reproducibility

All Forders contain a "Project.toml" and a "Manifest.toml" folder (there are identical across all chaters). These can be used to recreate the exact same package versions that I used to run them initially.


