## Fetch Packages ###
using Catalyst


### The Model Structure ###

# A structure to contain all the information of a biochemical reaction network model.
mutable struct Model
    system::ReactionSystem
    p_vals::Vector{Float64}
    original_p_vals::Vector{Float64}
    v_syms::Vector{Symbol}
    p_syms::Vector{Symbol}
    noise_scaling::Union{Vector{ModelingToolkit.Num},Nothing}
    u0_rre::Union{Nothing,Vector{Float64}}
    u0s_cle::Vector{Vector{Float64}}
    u0s_gillespie::Vector{Vector{Int64}}

    function Model(system::ReactionSystem,p::Vector{Float64};noise_scaling=get_default_noise_scaling(system))
        (length(system.ps)!=length(p)) && error("Parameter vector length missmatch.")
        new(system,copy(p),copy(p),map(i -> Symbol(system.states[i].val.f.name), 1:length(system.states)),map(i -> Symbol(system.ps[i]), 1:length(system.ps)),noise_scaling,nothing,[],[])
    end
end
# Finds the default option for noise scalling.
function get_default_noise_scaling(system)
    (:η == Symbol(system.ps[end])) && return fill((@variables η)[1],length(system.eqs))
    return nothing
end



### Auxiliary Functions ###

# Resets the parameter values of a model to their original values.
function reset!(model::Model)
    model.u0_rre = nothing
    model.u0s_cle = Vector{Vector{Float64}}()
    model.u0s_gillespie = Vector{Vector{Int64}}()
    model.p_vals = copy(model.original_p_vals);
end

# Uses symbols as indexing.
import Base.getindex, Base.setindex!
function getindex(model::Model,sym::Symbol)
    !in(sym,model.p_syms) && error("The model have no parameter $sym.")
    return getindex(model.p_vals,findfirst(sym.==model.p_syms))
end
function setindex!(model::Model,val,sym::Symbol)
    !in(sym,model.p_syms) && error("The model have no parameter $sym.")
    model.u0_rre = nothing
    model.u0s_cle = Vector{Vector{Float64}}()
    model.u0s_gillespie = Vector{Vector{Int64}}()
    return setindex!(model.p_vals,val,findfirst(sym.==model.p_syms))
end

# Finds the index of a specific parameter/variable.
get_par_idx(model::Model,p_sym::Symbol) = findfirst(model.p_syms .== p_sym)
get_var_idx(model::Model,v_sym::Symbol) = findfirst(model.v_syms .== v_sym)
