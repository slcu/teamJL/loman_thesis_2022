### Fetch Required Packages ###
using Plots, Plots.Measures
using BifurcationKit
using LinearAlgebra, Setfield


### Sets Default Stuff ###

# Sets the defaults.
gr(); default(fmt = :png)
default(framestyle=:box,grid=false,xguidefontsize=16,yguidefontsize=16,titlefontsize=18)

# Creates an empty plot (for creating spaces in sub plots).
p0 = plot(framestyle=:none); 
p01 = plot(framestyle=:none); p02 = plot(framestyle=:none); p03 = plot(framestyle=:none); p04 = plot(framestyle=:none); p05 = plot(framestyle=:none); p06 = plot(framestyle=:none); p07 = plot(framestyle=:none); p08 = plot(framestyle=:none); p09 = plot(framestyle=:none);
p001 = plot(framestyle=:none); p002 = plot(framestyle=:none); p003 = plot(framestyle=:none); p004 = plot(framestyle=:none); p005 = plot(framestyle=:none); p006 = plot(framestyle=:none); p007 = plot(framestyle=:none); p008 = plot(framestyle=:none); p009 = plot(framestyle=:none);


### Basic Plotting Functionality ###

# Plots a vertical line (to e.g. mark stress addition).
plot_vertical_line(args...;kwargs...) = (plot(); plot_vertical_line!(args...;kwargs...);)
plot_vertical_line!(x_value,heigth;linestyle=:dash,lw=3,color=:red,label="",kwargs...) = plot!([x_value,x_value],[0,heigth];lw=lw,color=color,linestyle=linestyle,label=label,kwargs...)
plot_vertical_lines(args...;kwargs...) = (plot(); plot_vertical_lines!(args...;kwargs...);)
function plot_vertical_lines!(x_values,heigth;linestyle=:dash,lw=3,colors=fill(:red,length(x_values)),label="",kwargs...)
    foreach(i->plot_vertical_line!(x_values[i],heigth;linestyle=linestyle,lw=lw,color=colors[i],label=label), 1:length(x_values))
    plot!(;kwargs...)
end


### Basic Simulation Plots ###

# Creates an activation simulation and plots it.
plot_activation(args...;kwargs...) = (plot(); plot_activation!(args...;kwargs...);)
function plot_activation!(p,tspan,stress_time,n;model=model3,color=:green,colors=fill(color,1,n),linealpha=0.6,lw=3,xticks=[],yticks=[],xguide="Time",yguide="σ",activation_line_width=4,ymax=Inf,title="",foreground_color_border=:black,titleloc=:center,plot_labels=true,legendfontsize=9,kwargs...)
    p_start = setindex!(copy(p),p[4],1)
    sols = monte(setindex!(copy(p),p[4],1),tspan,n,model=model,p_steps=(:S,(stress_time,p[1]-p[4])))
    plot!(sols,vars=[1],color=colors,linealpha=linealpha,lw=lw,label="",right_margin=2mm,xticks=xticks,yticks=yticks,xguide=xguide,yguide=yguide,ylimit=(0.,ymax),kwargs...)
    plot_labels && foreach(i -> plot!([],[],label="Simulation $i",color=colors[i],lw=lw,legend=:topright,legendfontsize=legendfontsize), 1:n)
    plot_vertical_line!(stress_time,(ymax==Inf ? maximum(vcat(map(i->first.(sols[i].u),1:n)...)) : ymax),lw=activation_line_width,foreground_color_border=foreground_color_border,title=title,titleloc=titleloc)
end

# Creates a simulation and plots it.
plot_simulation(args...;kwargs...) = (plot(); plot_simulation!(args...;kwargs...);)
function plot_simulation!(p,tspan,n;model=model3,init_fp=1,u0=sys_roots(model,p)[init_fp],color=:green,colors=fill(color,1,n),linealpha=0.6,lw=1,xticks=[],yticks=[],xguide="Time",yguide="σ",activation_line_width=4,label="",legendfontsize=9,ymax=Inf)
    sols = monte(p,tspan,n,model=model,init_fp=init_fp,u0=u0)
    plot!(sols,vars=[1],color=colors,linealpha=linealpha,lw=lw,right_margin=2mm,xticks=xticks,yticks=yticks,xguide=xguide,yguide=yguide,label="",legend=:topright,ylimit=(0.,ymax))
    plot!([],fill([],n),lw=lw,color=colors,label=label,legend=:topright,legendfontsize=legendfontsize)
end

### Basic Nullcline Plots ###

# Plots a single (non-trivial) nullcline.
plot_nullcline(args...;kwargs...) = (plot(); plot_nullcline!(args...;kwargs...);)
function plot_nullcline!(p;lw=4,color=:red,xmin=0.,xmax=Inf,ymin=0.,ymax=Inf,label="",linealpha=1.0,linestyle=:solid,kwargs...)
    zeros = nullcline_zeros(p)
    (length(zeros)==3) && plot!(nullcline(p),zeros[2],zeros[3];lw=lw,color=color,label="",kwargs...)
    plot!(nullcline(p),p[4],zeros[1];lw=lw,xguide="σ",yguide="A",xlimit=(xmin,xmax),ylimit=(ymin,ymax),label=label,color=color,linealpha=linealpha,linestyle=linestyle,kwargs...)
end

plot_nullclines(args...;kwargs...) = (plot(); plot_nullclines!(args...;kwargs...);)
function plot_nullclines!(p,target_par,vals;set_label=true,lw=4,colors=fill(:red,length(vals)),xmin=0.,xmax=Inf,ymin=0.,ymax=Inf,linealpha=1.0,linestyle=:solid,xguide="σ",yguide="A",kwargs...)
    labels = set_label ? map(val -> string(target_par)*" = "*string(val), vals) : fill("",length(vals))
    foreach(i->plot_nullcline!(setindex!(copy(p),vals[i],par2idx(target_par));lw=lw,label=labels[i],color=colors[i],xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,linealpha=linealpha,linestyle=linestyle,kwargs...), 1:length(vals))
    return plot!(xguide=xguide,yguide=yguide)
end

# Plots full nullcline set for single parameter value
make_nullcline(args...;kwargs...) = (plot(); make_nullcline!(args...;kwargs...);)
function make_nullcline!(p;lw=4,color1=:blue,color2=:red,label1="",label2="",xmin=0.,xmax=1.3,ymin=0.,ymax=Inf,linealpha=1.0,linestyle=:solid,kwargs...)
    plot!(x->x;lw=lw,xlimit=(0.,max(xmax,5.)),label=label1,color=color1,kwargs...)
    plot_nullcline!(p;lw=lw,xguide="σ",yguide="A",color=color2,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,label=label2,linealpha=linealpha,linestyle=linestyle,kwargs...)
end

# Plots full nullcline set, while varrying the value of a single parameter.
make_nullclines(args...;kwargs...) = (plot(); make_nullclines!(args...;kwargs...);)
function make_nullclines!(p,target_par,vals;lw=4,color1=:blue,colors2=fill(:red,length(vals)),label1="",set_label=true,xmin=0.,xmax=1.,ymin=0.,ymax=Inf,kwargs...)
    plot!(x->x;lw=lw,xlimit=(0.,max(xmax,5)),label=label1,color=color1,kwargs...)
    plot_nullclines!(p,target_par,vals;lw=lw,xguide="σ",yguide="A",set_label=set_label,colors=colors2,xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,kwargs...)
end


### Basic Bifurcation Plots ###

# Internal structure for storing the important information in a bifurcation diagram.
struct BifurcationDiagram
    branches::Vector{NamedTuple{(:p, :u, :stab),Tuple{Array{Float64,1},Array{Float64,1},Bool}}}

    # Generates a bifurcation diagram.
    function BifurcationDiagram(p, p_span;model=model3,par=:S,var=1)
        p_idx = par2idx(par)
        opts = ContinuationPar(ds=0.001, maxSteps = 100000, pMax = p_span[2], detectBifurcation=3) 
        DO = DeflationOperator(2.0, dot, 1., sys_roots(model,p)); 
        params_input = setindex!(copy(p),p_span[1],p_idx);
        cont_out, = continuation(get_F(model), get_J(model), params_input, (@lens _[p_idx]) ,opts , DO, verbosity = 0, showplot=false, printSolution=(x, p) -> x[var], perturbSolution = (x,p,id) -> (x  .+ 0.8 .* rand(length(x))), callbackN = (x, f, J, res, iteration, itlinear, options; kwargs...) -> res <1e7);
    
        branches = sort(filter(b->!isempty(b[1]), extract_path.(cont_out[1:end])), by = b->b[2][end])
        branch = NamedTuple{(:p, :u, :stabs)}(map(i->vcat(getindex.(branches,i)...), 1:3))
        idxs = [1,findall(i -> i[1]!=i[2], Pair.(branch[3][1:end-1],branch[3][2:end]))...,length(branch[3])]
        new(map(idx -> (p=branch.p[idx[1]:idx[2]],u=branch.u[idx[1]:idx[2]],stab=branch.stabs[idx[2]]), Pair.(idxs[1:end-1],idxs[2:end])))
    end
end
# Extracts and cleanes the interesting paths from the continuation algorithm output.
function extract_path(branch)
    any(getfield.(branch.branch,:x) .< 0) && return ([],[],[])
    output = [getfield.(branch.branch,:param), getfield.(branch.branch,:x), getfield.(branch.branch,:stable) .== 1]
    (output[2][1]>output[2][end]) && reverse!.(output)
    return output    
end;

# Plots a bifurcation diagram.
plot_bifurcation_diagram(bif::BifurcationDiagram;kwargs...) = (plot(); plot_bifurcation_diagram!(bif;kwargs...);)
function plot_bifurcation_diagram!(bif::BifurcationDiagram;stab_color=:blue,unstab_color=:red,stab_linestyle=:solid,unstab_linestyle=:dot,lw=3,xguide="S",yguide="σ",kwargs...)
    foreach(branch -> plot!(branch.p,branch.u,lw=lw,color=(branch.stab ? stab_color : unstab_color),linestyle=(branch.stab ? stab_linestyle : unstab_linestyle),label=""), bif.branches)
    return plot!(xguide=xguide,yguide=yguide;kwargs...)
end

# Creates (but do not return), and plots, a set of bifrucation diagrams.
make_bifurcation_diagrams(args...;kwargs...) = (plot(); make_bifurcation_diagrams!(args...;kwargs...);)
function make_bifurcation_diagrams!(p, p_span,par2,vals;model=model3,par1=:S,var=1,set_labels=false,xguide="S",yguide="σ",lw=3,stab_colors=fill(:blue,length(vals)),unstab_colors=fill(:red,length(vals)),stab_linestyles=fill(:solid,length(vals)),unstab_linestyles=fill(:dot,length(vals)),kwargs...)
    bifs = map(val -> BifurcationDiagram(setindex!(copy(p),val,par2idx(par2)), p_span;model=model,par=par1,var=var), vals)
    foreach(i -> plot_bifurcation_diagram!(bifs[i],lw=lw,stab_color=stab_colors[i],unstab_color=unstab_colors[i],stab_linestyle=stab_linestyles[i],unstab_linestyle=unstab_linestyles[i]), 1:length(bifs))
    set_labels && foreach(i -> plot!([],[],color=stab_colors[i],label=string(par2)*" = "*string(vals[i])), 1:length(bifs))
    return plot!(xguide=xguide,yguide=yguide;kwargs...)
end


### Composite Simulation-Nullcline Plots ###

# Creates a simulation, and plots it in phase-space.
function plot_simulation_space(p,tspan,stress_time;model=model3,color_prestress=:lightgreen,color_poststress=:darkgreen,linealpha=0.3,lw=1,xticks=[],yticks=[],saveat=0.05,lw_nc=5,color1=:blue,colors2=[:pink, :red],xmax=1.3,ymax=1.3,title="")
    p_start = setindex!(copy(p),p[4],1)
    sol = stochsim(setindex!(copy(p),p[4],1),tspan,model=model,p_steps=(:S,(stress_time,p[1]-p[4])),saveat=saveat);    
    plot(sol,vars=(1,length(model.states)),color=[fill(color_prestress,Int64(stress_time/saveat)+1)...,fill(color_poststress,length(sol.t)-Int64(stress_time/saveat)-1)...],linealpha=linealpha,lw=lw,xticks=xticks,yticks=yticks,xguide="σ",yguide="A",label="")
    return make_nullclines!(p,:S,[p[4],p[1]],color1=color1,colors2=colors2,lw=lw_nc,set_label=false,xmax=xmax,ymax=ymax,title=title)
end
# Creates a simulation, and plots it in phase-space. Here the system does not activate.
function plot_simulation_space(p,tspan;model=model3,trajectory_color=:darkgreen,linealpha=0.3,lw=1,xticks=[],yticks=[],saveat=0.05,lw_nc=5,color1=:blue,color2=:red,xmax=1.3,ymax=1.3,title="")
    sol = stochsim(p,tspan,model=model,saveat=saveat);    
    plot(sol,vars=(1,length(model.states)),color=trajectory_color,linealpha=linealpha,lw=lw,xticks=xticks,yticks=yticks,xguide="σ",yguide="A",label="")
    return make_nullcline!(p,color1=color1,color2=color2,lw=lw_nc,set_label=false,xmax=xmax,ymax=ymax,title=title)
end

# Creates a simulation, and plots it both over time and in phase-space.
function plot_simulation_space_n_time(p,tspan,stress_time;model=model3,color_prestress=[:lightblue :darkgoldenrod1],color_poststress=[:darkblue :darkorange3],ymax_temp=Inf,linealpha_temp=[0.6 0.7],lw_temp=[4 3],linealpha_spat=0.3,lw_spat=3,activation_line_width=4,xticks=[],yticks=[],saveat=0.05,lw_nc=5,la_nc=0.7,color1=:green,colors2=[:pink, :red],xmax=1.3,ymax=1.3,legendfontsize=9,plot_labels=true)
    p_start = setindex!(copy(p),p[4],1)
    sol = stochsim(setindex!(copy(p),p[4],1),tspan,model=model,p_steps=(:S,(stress_time,p[1]-p[4])),saveat=saveat);    
    plot(sol,vars=[1,2],color=[[fill(color_prestress[1],Int64(stress_time/saveat)+1)...,fill(color_poststress[1],length(sol.t)-Int64(stress_time/saveat)-1)...] [fill(color_prestress[2],Int64(stress_time/saveat)+1)...,fill(color_poststress[2],length(sol.t)-Int64(stress_time/saveat)-1)...]],linealpha=linealpha_temp,lw=lw_temp,xticks=xticks,yticks=yticks,xguide="Time",yguide="Concentration",label="",ylimit=(0.,ymax_temp))
    if plot_labels
        plot!([],[],label="σ, before stress",color=color_prestress[1],lw=lw_temp[1],linealpha=linealpha_temp[1]); plot!([],[],label="σ, after stress",color=color_poststress[1],lw=lw_temp[1]); 
        plot!([],[],label="A, before stress",color=color_prestress[2],lw=lw_temp[2],linealpha=linealpha_temp[2]); plot!([],[],label="A, after stress",color=color_poststress[2],lw=lw_temp[2],linealpha=linealpha_temp[2],legendfontsize=legendfontsize); 
    end
    p1 = plot_vertical_line!(stress_time,(ymax_temp==Inf ? maximum(first.(sol.u)) : max(ymax_temp,maximum(first.(sol.u)))),lw=activation_line_width)
    plot(sol,vars=(1,length(model.states)),color=[fill(color_prestress[1],Int64(stress_time/saveat)+1)...,fill(color_poststress[1],length(sol.t)-Int64(stress_time/saveat)-1)...],linealpha=linealpha_spat,lw=lw_spat,xticks=xticks,yticks=yticks,xguide="σ",yguide="A",label="")
    plot_labels && plot!([],[[],[]],label=["prestress" "poststress"],color=[color_prestress[1] color_poststress[1]],lw=lw_spat,legendfontsize=legendfontsize)
    p2 = make_nullclines!(p,:S,[p[4],p[1]],color1=color1,colors2=colors2,lw=lw_nc,linealpha=la_nc,set_label=false,xmax=xmax,ymax=ymax)
    return (p1,p2)
end


