### Fetch Required Packages ###
using Catalyst
using Polynomials
using OrdinaryDiffEq, StochasticDiffEq

### Fetches Models ###
include("Models.jl")


### System Functions ###

# An array of all ODEFunctions
odefuns = map(model -> ODEFunction(convert(ODESystem,model),jac=true), models)
# An array of all system functions.
sys_Fs = map(odefun -> (u,p) -> odefun(u,p,0), odefuns)
# An array of all jacobians.
sys_Js = map(odefun -> (u,p) -> odefun.jac(u,p,0), odefuns)

# Functions, taking a model and returning the appropriate function.
get_F(model) = sys_Fs[length(model.states)-1]
get_J(model) = sys_Js[length(model.states)-1]

# Function for the (non-trivial) nullcline.
function nullcline(p)
    S,D,τ,v0,n = p
    return σ -> (1/D)*max(((1/(σ-v0)-1)*(S*σ)^n-1),0.)^(1/n)
end

### System Properties ###

# Gets the value for which the system has a roots.
function sys_zeros(p)
    S,D,τ,v0,n = p
    coefficients = zeros(Int64(n)+2)
    coefficients[Int64(n)+2] = -S^n-D^n
    coefficients[Int64(n)+1] = v0*(S^n+D^n)+S^n
    coefficients[2] = -1
    coefficients[1] = v0
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(Polynomial(coefficients)))))
end

# Get system roots.
function sys_roots(model,p)
    return map(z -> fill(z,length(model.states)), sys_zeros(p))
end

# Gets the (local) min and max values of the nullcline.
function nc_local_min_max(p)
    S,D,τ,v0,n = p
    pol = Polynomial([v0+v0^2,1/n-2*v0-1,1])
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(pol))))
end

# For a given parameter set, given v0>nc_2_max_v0(p), finds the threshold between an active/inactive system.
function large_v0_inactive_active_threshold(n)
    return (n/4)+1/(4n)-0.5+(n-1)/(2*n)
end

# Finds the places where a nullcines intersects with zero.
function nullcline_zeros(p)
    S,D,τ,v0,n = p
    coefficients = zeros(Int64(n)+2)
    coefficients[Int64(n)+2] = -S^(n)
    coefficients[Int64(n)+1] = S^n * (1+v0)
    coefficients[2] = -1
    coefficients[1] = v0
    return sort(real.(filter(x->(imag(x)==0)&&real(x)>0.,roots(Polynomial(coefficients)))))
end


### System Simulation ###

# Makes a single of (CLE based SDE) simulations of the system.
function detsim(p, tspan; model=model3, init_fp=1, u0=sys_roots(model,p)[init_fp], solver=Rosenbrock23(), p_steps=(), v_steps=(), callbacks=(), saveat=0.1, maxiters=1e6, kwargs...)
    prob = ODEProblem(model,u0,tspan,deepcopy(p))
    sols = solve(prob,solver;callback=CallbackSet(par_steps(p_steps)...,var_steps(v_steps)...,callbacks...), tstops=find_tstops(p_steps,v_steps), maxiters=maxiters, saveat=saveat, kwargs...)
    return sols
end

# Makes a single of (CLE based SDE) simulations of the system.
function stochsim(p, tspan; model=model3, init_fp=1, u0=sys_roots(model,p)[init_fp], solver=ImplicitEM(), p_steps=(), v_steps=(), callbacks=(), saveat=0.1, maxiters=1e6, kwargs...)
    prob = SDEProblem(model,u0,tspan,deepcopy(p),noise_scaling=(ModelingToolkit.@parameters η)[1])
    sols = solve(prob,solver;callback=CallbackSet(positive_domain(),par_steps(p_steps)...,var_steps(v_steps)...,callbacks...), tstops=find_tstops(p_steps,v_steps), maxiters=maxiters, saveat=saveat, kwargs...)
    return sols
end

# Makes a set of (CLE based SDE) simulations of the system.
function monte(p, tspan, n; model=model3, init_fp=1, u0=sys_roots(model,p)[init_fp], eSolver=EnsembleThreads(), solver=ImplicitEM(), p_steps=(), v_steps=(), callbacks=(), maxiters=1e6, saveat=0.1, kwargs...)
    prob = SDEProblem(model,u0,tspan,deepcopy(p),noise_scaling=(ModelingToolkit.@parameters η)[1])
    ensemble_prob = EnsembleProblem(prob,prob_func=(p,i,r)->p)
    sols = solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(positive_domain(),par_steps(p_steps)...,var_steps(v_steps)...,callbacks...), tstops=find_tstops(p_steps,v_steps), maxiters=maxiters, saveat=saveat, kwargs...)
    return sols
end


### Callbacks ###

#A callback for keeping a simulation within the positive domain.
function positive_domain()
    condition(u,t,integrator) = minimum(u) < 0
    affect!(integrator) = integrator.u .= integrator.uprev
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end

# A callback for making a step increase in a single parameter.
function par_step(p_idx,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.p[p_idx] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
# A callback for making several step increases, potentially over several parameters.
function par_steps(p_steps)
    output = Vector{DiscreteCallback}()
    for steps in my_split(p_steps), step in steps[2:end]
        push!(output,par_step(par2idx(steps[1]),step...))
    end
    return output
end

# A callback for making a step increase in a single variable.
function var_step(v_idx,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.u[v_idx] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
# A callback for making several step increases, potentially over several variables.
function var_steps(v_steps)
    output = Vector{DiscreteCallback}()
    for steps in my_split(v_steps), step in steps[2:end]
        push!(output,var_step(var2idx(steps[1]),step...))
    end
    return output
end

# Splits an array, used to extract information from callback input.
function my_split(array)
    starts = findall(typeof.(array).==Symbol)
    ends = [starts[2:end]...,length(array)+1].-1
    return [array[starts[i]:ends[i]] for i in 1:length(starts)]
end

# Finds the tstops for a given callback vector.
function find_tstops(p_steps,v_steps)
    first.(filter(i->typeof(i)!=Symbol,[p_steps...,v_steps...]))
end




### Miscellaneous Utility Functions ###

# Finds the index corresponding to the target parameter (as a symbol) in the parameter array.
function par2idx(target_par)
    return findfirst([:S, :D, :τ, :v0, :n, :η] .== target_par)
end

# Finds the index corresponding to the target variable (as a symbol) in the variable array.
function var2idx(target_par)
    return findfirst([:σ, :A1, :A2, :A3, :A4, :A5] .== target_par)
end

# For a parameter set, returns the 4 paraemters τ, v0, n, η.
function get_τ_v0_n_η(parameters)
    (length(parameters)==4) && return parameters
    (length(parameters)==6) && return parameters[3:6]
    error("Paraemter entry set have weird length (nether 4 nor 6).")
end