### Fetch Required Packages ###
using Serialization
using StatsBase

### Behaviour Grid Structures ###

# The path to the behaviour files.
if isdir("../../../../../../mount")
    behaviour_path_1I = "../../../../../../mount/hpc_uni/projects/parameter_space_scan/"
    behaviour_path_3I = "../../../../../../mount/hpc_uni/projects/parameter_space_scan_3_intermediaries/"
else
    behaviour_path_1I = "../../../../../../home/SLCU/torkel.loman/mount/hpc_uni/projects/parameter_space_scan/"
    behaviour_path_3I = "../../../../../../home/SLCU/torkel.loman/mount/hpc_uni/projects/parameter_space_scan_3_intermediaries/"
end

# Contains a single grid of behaviours.
struct BehaviourGrid
    behaviours::Matrix{Symbol}
    S_grid::Array{Float64,1}
    D_grid::Array{Float64,1}
    params::Array{Float64,1}
    file_existed::Bool

    function BehaviourGrid(parameters,S_grid,D_grid,dataset_tag; bp=behaviour_path_3I)
        τ,v0,n,η = get_τ_v0_n_η(parameters)
        filename = bp*"Data/$(dataset_tag)/DeterminedBehaviour/behaviours_v0_$(v0)_n_$(n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n)_η_$(η).jls"
        behaviours = (isfile(filename) ? deserialize(filename) : fill(:empty_behaviour,length(S_grid),length(D_grid)))
        new(behaviours,S_grid,D_grid,parameters,isfile(filename))
    end
end

# Contains a grid of behaviour grids.
struct BehaviourGrids
    behaviour_grids::Matrix{BehaviourGrid}
    parameters::Vector{Float64}
    par1::Symbol
    par2::Symbol
    vals1::Vector{Float64}
    vals2::Vector{Float64}

    function BehaviourGrids(parameters,S_grid,D_grid,dataset_tag;bp=behaviour_path_3I,τ_values=[parameters[1]],v0_values=[parameters[2]],n_values=[parameters[3]],η_values=[parameters[4]])
        lengths = length.([τ_values,v0_values,n_values,η_values])
        (count(lengths .> 1) != 2) && error("Weird input vectors given")
        idx1 = findfirst(lengths .> 1)
        idx2 = findlast(lengths .> 1)
        
        behaviour_grids = Matrix{BehaviourGrid}(undef,lengths[idx1],lengths[idx2])
        for (τi,τ) in enumerate(τ_values), (v0i,v0) in enumerate(v0_values), (ni,n) in enumerate(n_values), (ηi,η) in enumerate(η_values)
            behaviour_grids[[τi,v0i,ni,ηi][idx1],[τi,v0i,ni,ηi][idx2]] = BehaviourGrid([τ,v0,n,η],S_grid,D_grid,dataset_tag,bp=bp)
        end
        return new(behaviour_grids,parameters,[:τ,:v0,:n,:η][idx1],[:τ,:v0,:n,:η][idx2],[τ_values,v0_values,n_values,η_values][idx1],[τ_values,v0_values,n_values,η_values][idx2])
    end
end



### Plots Behaviour Grid(s) ###

# Plots a single behaviour grid.
function plot_behaviour_grid(behaviour_grid::BehaviourGrid;start_s_slice=1,smooth=true,idx_axis=true,xticks=nothing,yticks=nothing,set_param_title=false,markers=[],marker_color=RGB{Float64}(0.,0.,0.),title="",xguide="",yguide="",kwargs...)
    set_param_title && (title="τ=$(behaviour_grid.params[1]), v0=$(behaviour_grid.params[2]), n=$(behaviour_grid.params[3]), η=$(behaviour_grid.params[4])")
    if idx_axis 
        (xticks===nothing) && (xticks = 10:20:length(D_grid))
        (yticks===nothing) && (yticks = 10:20:length(S_grid[start_s_slice:end]))
    else
        (xticks===nothing) && (xticks = (range(1, length(D_grid), length = 9), map(i->i[1:3],string.(10 .^(range(-1,stop=2,length=9))))))
        (yticks===nothing) && (yticks = (range(1, length(S_grid[start_s_slice:end]), length = 6), map(i->i[1:3],string.(10 .^(range(log10(S_grid[start_s_slice]),stop=2,length=6))))))
    end
    color_grid = get_color.(behaviour_grid.behaviours)[start_s_slice:end,1:end]
    p = plot((smooth ? smooth_colors(color_grid) : color_grid),yflip=false,aspect_ratio=:none,framestyle=:box,title=title,xguide=xguide,yguide=yguide,bottom_margin=3mm,left_margin=15mm,xticks=xticks,yticks=yticks,kwargs...)
    foreach(pos -> (p  = scatter!((pos[2],pos[1]),label="",color=marker_color)), markers)
    return p
end

# Plots a grid of behaviour grids.
function plot_behaviour_grids(bgs::BehaviourGrids;plot_size=(600,400),start_s_slice=1,smooth=true,idx_axis=true,xticks=[],yticks=[],set_param_title=false,set_param_axes=true,markers=[],marker_color=RGB{Float64}(0.,0.,0.),title="",xguide="",yguide="",kwargs...)
    plots = map(bg -> plot_behaviour_grid(bg,start_s_slice=start_s_slice,smooth=smooth,idx_axis=idx_axis,xticks=xticks,yticks=yticks,set_param_title=set_param_title,markers=markers,marker_color=marker_color,title=title,xguide=xguide,yguide=yguide), bgs.behaviour_grids)
    set_param_axes && foreach(i -> plots[i,1]=plot!(plots[i,1],title="$(bgs.par1) = $(bgs.vals1[i])"),1:size(plots)[1])
    set_param_axes && foreach(i -> plots[1,i]=plot!(plots[1,i],yguide="$(bgs.par2) = $(bgs.vals2[i])"),1:size(plots)[2])
    plot(plots...,size=plot_size,guidefontsize=13,titlefontsize=13,layout=reverse(size(plots)))
end


### Smooths the Colors of a Color Grid ###

# Smooths the colors, filtering out noise.
function smooth_colors(color_grid)
    return [get_major_color(get_colors_from_color_grid(color_grid,i,j)) for i in 1:size(color_grid)[1], j in 1:size(color_grid)[2]]
end

# For a small array of colors, find the dominating one (or pick the original one if none is dominating it enough).
function get_major_color(colors;color_original=colors[1])
    color_count_dict = countmap(colors)
    color_counts = sort(collect(color_count_dict),by=x->x[2])
    (color_counts[end][2] > color_count_dict[color_original]) && (return color_counts[end][1])
    return color_original
end

# For a given color grid and set of index, returns a (weigthed) list of the target, and it neighbours, colors.
function get_colors_from_color_grid(color_grid,i,j)
    colors = fill(color_grid[i,j],3)
    for I = max(i-1,1):1:min(i+1,size(color_grid)[1]), J = max(j-1,1):1:min(j+1,size(color_grid)[2])
        (I==i) && (J==j) && continue
        push!(colors,color_grid[I,J])
        (I==i || J==j) && push!(colors,color_grid[I,J])
    end
    return colors
end