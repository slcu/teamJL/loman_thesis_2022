### Parameter Evaluation Structures ###

# Contains the information regarding the steady state distribution.
struct SteadyStateDistributionData
    mean_activity::Float64
    std_activity::Float64
    median_activity::Float64
    mean_activity_A::Float64
    std_activity_A::Float64
    median_activity_A::Float64
    truncated_distribution_means::Vector{Float64}
    truncated_distribution_stds::Vector{Float64}
    truncated_distribution_means_A::Vector{Float64}
    truncated_distribution_stds_A::Vector{Float64}
    percentile_activities::Vector{Float64}
    percentile_activities_A::Vector{Float64}

    passage_counts::Union{Nothing,Vector{Int64}}

    perm_max_power::Float64
    perm_max_power_freq::Float64
    perm_max_power_A::Float64
    perm_max_power_freq_A::Float64
end

# Contains the information regarding the fixed point.
struct FPData
    fps_dist_type::Union{Nothing,Symbol}
    fps_dist::Union{Nothing,Vector{Vector{Float64}}}
    fps_m_type::Union{Nothing,Symbol}
    fps_m::Union{Nothing,Vector{Vector{Float64}}}
    fps_type::Union{Nothing,Symbol}
    fps::Union{Nothing,Vector{Vector{Float64}}}
end

# Contains the information regarding system transitions.
struct TransitionData
    initial_activation_type::Symbol
    initial_activation_times::Vector{Float64}
    initial_activation_MaxIters::Bool
    initial_pulse_activation_type::Symbol
    initial_pulse_activation_times::Vector{Float64}
    initial_pulse_activation_MaxIters::Bool
    deactivation_type::Symbol
    deactivation_times::Vector{Float64}
    deactivation_MaxIters::Bool
    reactivation_type::Symbol
    reactivation_times::Vector{Float64}
    reactivation_MaxIters::Bool
end

# Contains the information from the evaluation of a single parameter set.
mutable struct ParameterEvaluation
    p::Vector{Float64}
    rts::Vector{Float64}
    max_real_eigen_vals::Vector{Float64}
    stabs::Vector{Bool}
    nc_min_max::Vector{Float64}
    det_fps_type::Symbol

    sols_data::Vector{NamedTuple{(:seed, :maxval, :retcode, :end_val),Tuple{UInt64,Float64,Symbol,Float64}}}
    steady_state_distribution_data::Union{Nothing,SteadyStateDistributionData}
    fp_data::Union{Nothing,FPData}
    transition_data::Union{Nothing,TransitionData}
    steady_state_distribution_reduced_data::Vector{SteadyStateDistributionData}

    behaviour::Symbol
    m::Int64
    l::Float64
    is_default::Bool
    default::Symbol
    additional_information::Vector{Any}
end


### Loads & Plots Evaluation Grids ###

# Contains a single grid of evaluation.
struct EvaluationGrid
    evaluations::Matrix{Union{Nothing,ParameterEvaluation}}
    S_grid::Array{Float64,1}
    D_grid::Array{Float64,1}
    params::Array{Float64,1}
    file_existed::Bool

    function EvaluationGrid(parameters,S_grid,D_grid,dataset_tag; bp=behaviour_path_3I)
        τ,v0,n,η = get_τ_v0_n_η(parameters)
        filename = bp*"Data/$(dataset_tag)/FullEvaluation/behaviours_v0_$(v0)_n_$(n)_η_$(η)/grid_τ_$(τ)_v0_$(v0)_n_$(n)_η_$(η).jls"
        evaluations = (isfile(filename) ? deserialize(filename) : fill(nothing,length(S_grid),length(D_grid)))
        new(evaluations,S_grid,D_grid,parameters,isfile(filename))
    end
end

# Contains a grid of evaluation grids.
struct EvaluationGrids
    evaluation_grids::Matrix{EvaluationGrid}
    parameters::Vector{Float64}
    par1::Symbol
    par2::Symbol
    vals1::Vector{Float64}
    vals2::Vector{Float64}

    function EvaluationGrids(parameters,S_grid,D_grid,dataset_tag;bp=behaviour_path_3I,τ_values=[parameters[1]],v0_values=[parameters[2]],n_values=[parameters[3]],η_values=[parameters[4]])
        lengths = length.([τ_values,v0_values,n_values,η_values])
        (count(lengths .> 1) != 2) && error("Weird input vectors given")
        idx1 = findfirst(lengths .> 1)
        idx2 = findlast(lengths .> 1)
        
        evaluation_grids = Matrix{EvaluationGrid}(undef,lengths[idx1],lengths[idx2])
        for (τi,τ) in enumerate(τ_values), (v0i,v0) in enumerate(v0_values), (ni,n) in enumerate(n_values), (ηi,η) in enumerate(η_values)
            evaluation_grids[[τi,v0i,ni,ηi][idx1],[τi,v0i,ni,ηi][idx2]] = EvaluationGrid([τ,v0,n,η],S_grid,D_grid,dataset_tag,bp=bp)
        end
        return new(evaluation_grids,parameters,[:τ,:v0,:n,:η][idx1],[:τ,:v0,:n,:η][idx2],[τ_values,v0_values,n_values,η_values][idx1],[τ_values,v0_values,n_values,η_values][idx2])
    end
end



### Plots Evaluation Grid(s) ###

# Plots a single evaluation grid.
function plot_evaluation_grid(evaluation_grid::EvaluationGrid;coloring_function=find_color,start_s_slice=1,smooth=true,idx_axis=true,xticks=nothing,yticks=nothing,set_param_title=false,markers=[],marker_color=RGB{Float64}(0.,0.,0.),title="",xguide="",yguide="",kwargs...)
    set_param_title && (title="τ=$(evaluation_grid.params[1]), v0=$(evaluation_grid.params[2]), n=$(evaluation_grid.params[3]), η=$(evaluation_grid.params[4])")
    if idx_axis 
        (xticks===nothing) && (xticks = 10:20:length(D_grid))
        (yticks===nothing) && (yticks = 10:20:length(S_grid[start_s_slice:end]))
    else
        (xticks===nothing) && (xticks = (range(1, length(D_grid), length = 9), map(i->i[1:3],string.(10 .^(range(-1,stop=2,length=9))))))
        (yticks===nothing) && (yticks = (range(1, length(S_grid[start_s_slice:end]), length = 6), map(i->i[1:3],string.(10 .^(range(log10(S_grid[start_s_slice]),stop=2,length=6))))))
    end
    color_grid = coloring_function.(evaluation_grid.evaluations)[start_s_slice:end,1:end]
    p = plot((smooth ? smooth_colors(color_grid) : color_grid),yflip=false,aspect_ratio=:none,framestyle=:box,title=title,xguide=xguide,yguide=yguide,bottom_margin=3mm,left_margin=15mm,xticks=xticks,yticks=yticks,kwargs...)
    foreach(pos -> (p  = scatter!((pos[2],pos[1]),label="",color=marker_color)), markers)
    return p
end

# Plots a grid of evaluation grids.
function plot_evaluation_grids(egs::EvaluationGrids;coloring_function=find_color,plot_size=(600,400),start_s_slice=1,smooth=true,idx_axis=true,xticks=[],yticks=[],set_param_title=false,set_param_axes=true,markers=[],marker_color=RGB{Float64}(0.,0.,0.),title="",xguide="",yguide="",kwargs...)
    plots = map(eg -> plot_evaluation_grid(eg,coloring_function=coloring_function,start_s_slice=start_s_slice,smooth=smooth,idx_axis=idx_axis,xticks=xticks,yticks=yticks,set_param_title=set_param_title,markers=markers,marker_color=marker_color,title=title,xguide=xguide,yguide=yguide), egs.evaluation_grids)
    set_param_axes && foreach(i -> plots[i,1]=plot!(plots[i,1],title="$(egs.par1) = $(egs.vals1[i])"),1:size(plots)[1])
    set_param_axes && foreach(i -> plots[1,i]=plot!(plots[1,i],yguide="$(egs.par2) = $(egs.vals2[i])"),1:size(plots)[2])
    plot(plots...,size=plot_size,guidefontsize=13,titlefontsize=13,layout=reverse(size(plots)))
end

# Basic coloring function.
function find_color(pe::Union{Nothing,ParameterEvaluation})
    isnothing(pe) && return RGB{Float64}(1.,1.,1.)
    return get_color(pe.behaviour)
end
# Coloring function which splits stochastic pulsing into with and without initial pulse.
function find_color_separated_sp(pe::Union{Nothing,ParameterEvaluation})
    isnothing(pe) && return RGB{Float64}(1.,1.,1.)
    (pe.behaviour == :stochastic_pulsing) && (!isnothing(pe.transition_data.initial_activation_type)) &&  (pe.transition_data.initial_activation_type == :instantaneous) && return RGB{Float64}(1.,165. /255.,0.)
    return get_color(pe.behaviour)
end;