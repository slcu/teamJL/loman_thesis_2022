## Fetch Packages ###
using StochasticDelayDiffEq


### Declare the SDDE Model ###
function SDDE_model_f(du,u,h,p,t)
    S,D,τ,v0,n,η = p
    σ, = u
    A = h(p,t-τ)[1]
    du[1] = v0 + ((S*σ)^n) / ((S*σ)^n + (D*A)^n + 1) - σ
end
function SDDE_model_g(du,u,h,p,t)
    S,D,τ,v0,n,η = p
    σ, = u
    A = 0*h(p,t-τ)[1]
    du[1,1] = η*sqrt(v0 + ((S*σ)^n) / ((S*σ)^n + (D*A)^n + 1))
    du[1,2] = -η*sqrt(σ)
end
function make_SDDE_model_h(u0)
    return (p,t) -> [0.1]
end


### Simulation Function ###

# Single activation using the CLE interpretation and the discrte delay model.
function sdde_activation(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64; saveat=0.1, maxiters=1e6,dt=0.0025, adaptive=false,
        u0 = stochsim(model1,setindex!(copy(p_vals),0.,1),(0.,500.),u0=fill(p_vals[4],2),adaptive=adaptive,maxiters=maxiters,saveat=saveat,dt=dt).u[end][1:1], 
        kwargs...)
    sdde_prob = SDDEProblem(SDDE_model_f, SDDE_model_g, u0, make_SDDE_model_h(u0), (-pre_stress_t,post_stress_t), setindex!(copy(p_vals),0.,1), noise_rate_prototype=zeros(1,2));
    solve(sdde_prob,ImplicitEM();callback=CallbackSet(positive_domain(),par_step(1,0.0,p_vals[1])),adaptive=adaptive,maxiters=maxiters,saveat=saveat,dt=dt,tstops=[0.0],kwargs...)
end


### Plotting Function ###

# (Simulates and) Plots a single activation simulation.# Plots the activations of a monte carlo assemble of trajectories.
plot_sdde_activation(args...;kwargs...) = (plot(); plot_sdde_activation!(args...;kwargs...);)
function plot_sdde_activation!(p_vals::Vector{Float64}, pre_stress_t::Float64, post_stress_t::Float64;ymax=Inf,color=:darkblue,lw=4,la=0.6,activation_lw=7,activation_la=0.8,xticks=[],yticks=[],saveat=0.05, maxiters=1e6,dt=0.0025, adaptive=false,xguide="Time",yguide="Concentration",label="",
        u0 = stochsim(model1,setindex!(copy(p_vals),0.,1),(0.,500.),u0=fill(p_vals[4],2),adaptive=adaptive,maxiters=maxiters,saveat=saveat,dt=dt).u[end][1:1], 
        kwargs...)
    sol = sdde_activation(p_vals, pre_stress_t, post_stress_t; saveat=saveat, maxiters=maxiters, adaptive=adaptive, dt=dt, u0=u0)
    plot!(sol;color=color,la=la,lw=lw,xticks=xticks,yticks=yticks,xguide=xguide,yguide=yguide,label=label,ylimit=(0.,ymax),kwargs...)
    plot_vertical_line!(0,(ymax==Inf ? 1.05*maximum(first.(sol.u)) : max(ymax,1.05*maximum(first.(sol.u)))),lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end;